### Overview

The API and SDKs use token-based authentication. There are two types of authentication tokens available: `API KEY` and `SDK TOKEN`. 

##### API Key

API keys must be included in the header of all requests made to the API. 

```
API keys should be protected and must never be used in the frontend of the application. They should only be used on the server. API key access should be limited to only the minimum number of people necessary and periodically rotated.
```

##### SDK Token

All of the SDKs authenticate using SDK tokens, which are represented as JSON Web Tokens (JWT).

SDK tokens are restricted to an individual persona (applicant) and expire after 90 minutes, and therefore can be safely used on the client side (frontend or mobile application).

```
API keys cannot be used to authenticate the SDKs.
```

SDK is only responsible for capturing and uploading photos/videos, you would need to start a verification check on your server once the SDK flow is complete.



### Create a persona

A Hive Verify persona must be created for each applicant before starting the SDK flow. You must create personas from your server using the **API Key**. 

##### POST /personas

##### Authorization: Key <API_KEY>



##### Request Body Parameters

```json
{
  "first_name": "John",
  "last_name": "Smith",
  "email": "john.smith@hive.id",
  "dob": ""
}
```

| Parameter  | Type   | Description                                         |
| ---------- | ------ | --------------------------------------------------- |
| first_name | String | The applicant's first name. `REQUIRED`              |
| last_name  | String | The applicant's last name. `REQUIRED`               |
| email      | String | The applicant's email address                       |
| dob        | String | The applicant's date of birth in dd-mm-yyyy format. |

##### Response Body Parameters

```json
{
  "request_id": "dd3b528d-b0f8-4b2a-a469-e094bfc69325",
  "persona_id": "55b09cd2-daae-4a73-9d73-2b45d7f80c8d"
}
```

| Parameter  | Type   | Description                                                  |
| ---------- | ------ | ------------------------------------------------------------ |
| request_id | String | Unique value to help trace the requests, automatically appended to each response. |
| persona_id | String | The identifier of the created persona object                 |



### Create a session

A session must be created **BEFORE** the SDK is initialized. You must create sessions from your server using the **API Key**. 

Generates a short-lived SDK token. Returns a `session_token` object containing the SDK token.

```
A session must be created and a new SDK token included every time the SDK is initialized.
```

##### POST /sessions

##### Authorization: Key <API_KEY>



##### Request Body Parameters

```json
{
  "persona_id": "55b09cd2-daae-4a73-9d73-2b45d7f80c8d",
  "application_id": "9eda2b2c-b429-4383-8b45-15f45ba0e822"
}
```

| Parameter      | Type   | Description                                                  |
| -------------- | ------ | ------------------------------------------------------------ |
| persona_id     | String | Specifies the applicant for the verification session.        |
| application_id | String | The identifier of the application, which belongs to this service provider. |

##### Response Body Parameters

```json
{
  "request_id": "72a418f2-7ffc-46a0-8dc1-4750bceb4c64",
  "session_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uX2lkIjoiY2JiOWU4MWQtNjQ2Yy00MjJmLWJmNzktOWYzY2EyZGI0OGM4IiwiaWF0IjoxNTg1Mjc4NTk2LCJleHAiOjE1ODUyODIxOTZ9.ELb8LtLabqz-pQR2hUZh6aEWAQU2VFYm3CPkeXm0CxU"
}
```

| Parameter     | Type   | Description                                                  |
| ------------- | ------ | ------------------------------------------------------------ |
| request_id    | String | Unique value to help trace the requests, automatically appended to each response. |
| session_token | String | JWT containing session_id                                    |



### Retrieve a template

##### GET /sessions/{session_id}/templates

##### Authorization: Bearer <SDK_TOKEN>

Example: GET /sessions/cbb9e81d-646c-422f-bf79-9f3ca2db48c8/templates?locale=en

##### Request Parameters

| Parameter | Type   | Description                          |
| --------- | ------ | ------------------------------------ |
| locale    | String | Specifies the locale of the template |

##### Response Body

```json
{
  "id":"3fd22d7b-b86f-4874-9b2b-2ced4dc54566",
  "version":1,
  "whitelabel":true,
  "steps":{
    "welcome":{
      "kind":"welcome",
      "options":{
        "title":"Let’s get you verified",
        "subtitle":"Demo Inc would like to confirm your identity",
        "hint":{
          "email":"You will be asked to provide your email address",
          "document":"You will be asked to take pictures of your identity document",
          "face":"You will be asked to take a selfie"
        },
        "description":"The process should take less than a minute",
        "privacy_policy":{
          "title":"Privacy Policy",
          "link":"https://hive.id/privacy-policy"
        },
        "button":"Start"
      }
    },
    "email":{
      "kind":"email",
      "options":{
        "disable":true,
        "title":"Email address",
        "subtitle":"Please enter your email address",
        "placeholder":"name@example.com",
        "button":"Continue"
      }
    },
    "document":{
      "kind":"document",
      "options":{
        "first_page":{
          "title":"Identity document",
          "subtitle":"Get ready to take a photo of your ID",
          "hint0":"Position your identity document within the frame and take a picture",
          "hint1":"Make sure the details are clear to read with no blur or glare",
          "button":"Continue"
        },
        "next_page":{
          "title":"Identity document",
          "subtitle":"Get ready to take a photo of the back of your ID",
          "hint0":"Position your identity document within the frame and take a picture",
          "hint1":"Make sure the details are clear to read with no blur or glare",
          "button":"Continue"
        },
        "photo":{
          "title":"Identity document",
          "description":"Position the document with your photo in the frame"
        },
        "readability":{
          "title":"Identity document",
          "description":"Is the text clear and readable?",
          "button_continue":"Continue",
          "button_redo":"Take a new picture"
        },
        "loading":"Please wait while your photo is being processed"
      }
    },
    "face":{
      "kind":"face",
      "options":{
        "disable":false,
        "onboarding":{
          "title":"Take a selfie",
          "subtitle":"Get ready to take a photo of yourself",
          "hint0":"Position your face in the frame and take a picture",
          "hint1":"Hold your device on eye level and look straight into the camera",
          "button":"Continue"
        },
        "photo":{
          "title":"Selfie",
          "description":"Position your face in the frame and make sure it is clearly visible"
        },
        "readability":{
          "title":"Selfie",
          "description":"Is your face clearly visible and in the frame?",
          "button_continue":"Continue",
          "button_redo":"Take a new picture"
        },
        "loading":"Please wait while your photo is being processed"
      }
    },
    "complete":{
      "kind":"complete",
      "options":{
        "title":"Thank you",
        "subtitle":"You’re all set, we received your data and started the verification process.",
        "button":"Done",
        "upload_photos":"Please wait while we upload your photos"
      }
    }
  },
  "theme":{
    "dark_mode": true,
    "light": {
      "primary":"#6515DD",
      "secondary":"#D8D8D8",
      "text":"#414042",
      "background":"#ffffff",
      "inverted_text":"#ffffff",
      "email_placeholder": "#B7B7B7",
      "error_accent":"#EB6E6E",
      "error_text":"#ffffff",
      "loader_primary":"#6515DD",
      "loader_secondary":"#DFCFF8",
      "whitelabel":"#7D7D7D"
    },
    "dark": {
      "primary":"#6515DD",
      "secondary":"#D8D8D8",
      "text":"#414042",
      "background":"#ffffff",
      "inverted_text":"#ffffff",
      "email_placeholder": "#B7B7B7",
      "error_accent":"#EB6E6E",
      "error_text":"#ffffff",
      "loader_primary":"#6515DD",
      "loader_secondary":"#DFCFF8",
      "whitelabel":"#7D7D7D"
    }
  },
  "errors":{
    "session_expired":{
      "title":"Your session has expired",
      "description":""
    },
    "session_canceled":{
      "title":"Session has been canceled",
      "description":""
    },
    "email_not_valid":{
      "title":"The email is not valid",
      "description":""
    },
    "doc_not_found":{
      "title":"No valid document found",
      "description":""
    },
    "doc_type_not_supported":{
      "title":"The document is not supported",
      "description":""
    },
    "doc_blurry":{
      "title":"The picture looks blurry",
      "description":""
    },
    "doc_glare detected":{
      "title":"There was glare detected",
      "description":""
    },
    "face_not_found":{
      "title":"No face found",
      "description":""
    },
    "face_sunglasses_detected":{
      "title":"Sunglasses detected",
      "description":""
    },
    "multiple_faces_detected":{
      "title":"Multiple faces detected",
      "description":""
    },
    "low_light":{
      "title":"The picture looks dark",
      "description":""
    }
  }
}
```



### Add person credentials

##### POST /sessions/{session_id}/credentials

##### Authorization: Bearer <SDK_TOKEN>



##### Request Body Parameters

```json
{
  "type": "email",
  "data": "johnsmith@hive.id",
  "typing": ""
}
```

| Parameter | Type   | Description                                       |
| --------- | ------ | ------------------------------------------------- |
| type      | String | The type of credential. One of `email` or `phone` |
| data      | String | Credential value                                  |
| typing    | String |                                                   |

##### Response Body Parameters

##### 201 Created

```json
{
  "id": "9ac7f636-8236-4aa5-a8b9-410f81bc499f"
}
```

| Parameter | Type   | Description                      |
| --------- | ------ | -------------------------------- |
| id        | String | The identifier of the credential |

##### 400 Bad Request

```json
{
  "request_id": "e0af120d-7ac3-47ff-8c6c-9c1baf1c2a5e",
  "error": true,
  "error_code": "string"
}
```

| Parameter  | Type    | Description                                                  |
| ---------- | ------- | ------------------------------------------------------------ |
| request_id | String  | Unique value to help trace the requests, automatically appended to each response. |
| error      | Boolean |                                                              |
| error_code | String  | Machine readable representation of the error code to be passed back to the application or shown to the user. Please refer to the `Errors` section. |



### Upload a file

Uploads a file as part of a multipart request. The endpoint is used to upload both identity documents and selfie pictures. 

##### POST /sessions/{session_id}/files

##### Content-Type: multipart/form-data

##### Authorization: Bearer <SDK_TOKEN>



##### Request Body Parameters

```json
{
  "type": "identity_document",
  "file": "document.png"
}
```

| Parameter | Type   | Description                                                  |
| --------- | ------ | ------------------------------------------------------------ |
| type      | String | The type of file being uploaded. One of `identity_document` or `face` |
| file      | Binary | File being uploaded. Valid file formats are `jpg` or `png`   |

##### Response Body Parameters

##### 200 OK

```json
{
  "request_id": "c05a86f1-5c2f-42c4-8d0b-d7663078345a",
  "file_id": "db5eca58-547f-4cc4-b206-7c4ab02b45f2",
  "next_required": true
}
```

| Parameter     | Type    | Description                                                  |
| ------------- | ------- | ------------------------------------------------------------ |
| request_id    | String  | Unique value to help trace the requests, automatically appended to each response. |
| file_id       | String  | The identifier of the uploaded file                          |
| next_required | Boolean | For some document types, both sides are required for processing. If `true`, next_page view should be shown to the user (please refer to document object in the Template above). |

##### 400 Bad Request

```json
{
  "request_id": "e0af120d-7ac3-47ff-8c6c-9c1baf1c2a5e",
  "error": true,
  "error_code": "string"
}
```

| Parameter  | Type    | Description                                                  |
| ---------- | ------- | ------------------------------------------------------------ |
| request_id | String  | Unique value to help trace the requests, automatically appended to each response. |
| error      | Boolean |                                                              |
| error_code | String  | Machine readable representation of the error code to be passed back to the application or shown to the user. Please refer to the `Errors` section. |



### Change session status

A session must be completed once all the files have been uploaded.

##### PATCH /sessions/{session_id}

##### Authorization: Bearer <SDK_TOKEN>

Example: PATCH /sessions/cbb9e81d-646c-422f-bf79-9f3ca2db48c8

##### Request Body Parameters

```json
{
  "status": "complete",
  "files": [
    "db5eca58-547f-4cc4-b206-7c4ab02b45f2",
    "1b8b4a6a-c5b3-4949-9a45-6015ab4cd3d0"
  ]
}
```

| Parameter | Type   | Description                                                  |
| --------- | ------ | ------------------------------------------------------------ |
| status    | String | The status of session. One of `complete` or `canceled`       |
| files     | Array  | Array of all uploaded file identifiers in the current session. Required if status is `complete` |

##### Response Body Parameters

```json
{
  "request_id": "f8d1c737-df8f-4b19-a252-47766606378a",
  "status": "ok"
}
```

| Parameter  | Type   | Description                                                  |
| ---------- | ------ | ------------------------------------------------------------ |
| request_id | String | Unique value to help trace the requests, automatically appended to each response. |
| status     | String |                                                              |
