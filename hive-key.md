### Changelog

#### 03/02/2020

- Changed:

  - **2.3. Create a user:** The POST method is used whenever a new user is created. Can be used on mobile or web. The new user will still have to be paired with a mobile device before using Hive Key SDK.

    **Relative path**
    **POST /users/initiate --> POST /users**

    **Request payload**
    Removed `requesting_device` as obsolete.
    Moved `auth_data` and `device_details` to device pairing (2.4) 

    **Response payload**
    Removed `session_id` and `device_id` as obsolete.
    Added `status`. 

  - **2.4. Pair a device:** The POST method is used in order to pair the created user with a device.

    **Relative path**
    **POST /users/enroll --> POST /users/{user_id}/pairings**

    **Request payload**
    Added `auth_data` and `device_details` from 2.3
    Removed `session_id` and `device_id` as obsolete.

    **Response payload**
    Added `device_id`.

  - **3.3. Start a new authentication:** The POST authentication method is used to start a new authentication both on mobile and web.

    **Relative path**
    **POST /auth/start --> POST /authentications**

    **Request payload**
    Removed `device_details` as obsolete.
    Added `type`.

    **Response payload**
    Changed `session_id` to `authentication_id`.
    Added `status`.

  - **3.4. Complete authentication:** The PUT authentication method is used to finish the authentication.

    **Relative path**
    **POST /auth/finish --> PUT /authentications/{authentication_id}**

    **Request payload**
    Removed `session_id` as obsolete.

    **Response payload**
    Changed value of `status` from `ok` to `APPROVED`.

  - **4. Push Notifications** renamed to **Devices**. Push notification section moved to 4.2.

- Added:

  - **3.5. Cancel authentication:** The DELETE authentication method is used when a user cancels an authentication in progress.

    **Relative path**
    **DELETE /authentications/{authentication_id}**

  - **4.1. Update a device of a user:** The PATCH method is used to update the trusted device of the user.

    **Relative path**
    **PATCH /devices/{device_id}**

### 1. Authentication Headers

#### 1.1. Request Authorization Header

Any request to authentication server API is valid only when it contains an authorization header. Any attempt to send requests without fulfilling this requirement, results in an “unauthorized” (401) response. In addition, each response from the server contains a dedicated header, which allows the SDK to verify the server response.

**1.1.1. Construct a canonical string**:

The general canonical string format is: 

`HTTPRequestMethod:Host:CanonicalURI:HashedRequestPayload`

Example:

```
POST:sdk.hive.id:/key/v1/users/enroll:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
```

Where:

**HTTPRequestMethod** = `POST`

**Host** = `sdk.hive.id` or `sdk.eu.hive.id` or `sdk.au.hive.id`

**CanonicalURI** = `/key/v1/users/enroll`

**HashedRequestPayload** = `e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`

HashedRequestPayload is calculated as `HexEncode(Hash(RequestPayload))`. Hash function = `SHA256`, HexEncode = base-16 encoding of the digest in lowercase characters.

**1.1.2. Construct a signed JWT which contains the canonical string as its payload**:

The authorization header forms a signed JWT. The JWT contains the following:

- JWT Payload:

The JWT payload is the hashed (SHA256) canonical string mentioned in the previous step, represented in HEX encoding. For the above example:

HexEncode(Hash(`POST:sdk.hive.id:/key/v1/users/enroll:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`)) = `3150f550ae8c5e2ac51d3a84798e4106f156b3d8d65456e001bc23b3b6bc7db4`.

The JWT payload should have the following JSON format:

```json
{
    payload: "<hashed digest of the canonical string>"
}
```

- JWT Headers:

The JWT headers section should follow the following JSON format:

```json
{
    "alg": "HS256" ,
    "typ": "JWT" ,
    "client_id":"<client_id>",
    "application_id": "<application_id>",
}
```

| Component      | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| alg            | Specifies HS256 (HMAC SHA256) as the signing algorithm.      |
| typ            | This value identifies that the format of the message is JWT. |
| client_id      | The identifier of the Hive Key SDK tenant (service provider). |
| application_id | The identifier of the application, which belongs to this service provider. Can be obtained from the admin console and passed to SDK as a parameter. |

- JWT Signing:

The sender must sign the JWT using the API key, which can be obtained from the admin console and passed to SDK as a parameter.

**1.1.3. Construct the authorization header**:

The authorization header value is the signed JWT, as constructed in the previous steps.

Add the signed JWT as the request authorization header, with the `HIVEID-AUTH` prefix.

```
Authorization: HIVEID-AUTH <signed JWT value>
```

#### 1.2. Response Verification

Every response returned from the server should be verified by the SDK. The server uses a dedicated response header: `X-HIVEID-Signature`. This header value is a JWT, which is signed in a similar manner as explained above.

The SDK should verify the response as follows:

- Verify the signed JWT which is returned in the `X-HIVEID-Signature` header.
- Verify that the hashed response body is equal to the returned value in the JWT payload. In order to do this comparison, follow these steps:
  - Retrieve the hashed response body from the JWT which exists in the JWT payload: The value of the `payload` JSON field.
  - Hash the body of the response: The hashed response body = `HexEncode (Hash(response body))`
  - Compare the hashed response body in the JWT with the actual response body.

### 2. Registration and pairing user flows

##### 2.1. Passwordless registration

On first registration, user is creating a passwordless account.

<img src="/Users/znatokov/Desktop/hive/UML Sequence_ User Login Overview - UML Sequence_ User Login Overview.png" alt="UML Sequence_ User Login Overview - UML Sequence_ User Login Overview" style="zoom: 33%;" />

##### 2.2. Convert traditional to passwordless

User logs in to the application using traditional email/phone and password and is promted to enroll in passwordless authentication.

<img src="/Users/znatokov/Desktop/hive/UML Sequence_ User Login Overview - Page 2.png" alt="UML Sequence_ User Login Overview - Page 2" style="zoom:33%;" />





##### 2.3. Initiate Registration

##### POST /users

During the manual flow, the call to Initiate Registration causes Hive to send an OTP to the user via email, SMS or voice message, as indicated in the call parameters. The user must then enter the OTP manually to the service provider’s application or follow a deep link, which should then call Finalize Registration while providing the OTP, to complete the pairing process.

During the automatic flow, the OTP can be omitted and Finalize Registration is called without user intervention.

##### Request Body Parameters

```json
{
  "client_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "application_id": "4df5f945-5fe8-48fb-8a5a-0a6b71da42de",
  "flow": "manual",
  "type": "sms",
  "data": "+3800000000",
  "ip_address": "127.0.0.1",
  "typing_pattern": "",
  "user_data": { ... }
}
```

| Parameter | Type| Description |
|---|---|---|
| client_id | String | The identifier of the service provider account. |
| application_id | String | The identifier of the application, which belongs to this service provider. |
| flow | String | The type of flow to be used for pairing - `manual` or `automatic`. |
| type | String | The type of device that will be paired. One of: <br>- `sms` - send an OTP in a text message<br>- `voice` - send an OTP in a voice message<br>- `email` - send an OTP in an email message |
| data | String | The message target, whose value depends on the type value.<br>- `sms` or `voice`: Provide an international phone number<br>- `email`: Provide an email address |
| ip_address | String | The requesting device’s IP address. |
| typing_pattern | String | Typing pattern data as returned by TypingDNA SDK |
| user_data | Object | Optional. The value is passed to service provider’s application unchanged. |

##### Response Body Parameters

```json
{
  "request_id": "f3159e6b-5f56-47ca-b9ad-9df048934efa",
  "user_id": "fd031a39-a712-4081-a8b6-b72df64a170e",
  "status": "NOT_ACTIVE"
}
```

| Parameter  | Type   | Description                                                  |
| ---------- | ------ | ------------------------------------------------------------ |
| request_id | String | Unique value to help trace the requests, automatically appended to each response. |
| user_id    | String | A user ID value that has been created                        |
| status     | String | The user's status in Hive Key.<br />Valid values:<br />- `NOT_ACTIVE`: The user has no paired devices.<br />- `ACTIVE`: The user has at least one paired device for an application.<br />- `SUSPENDED`: The user is blocked from device enrollment or authentication. |



##### 2.4. Finalize Registration (device pairing)

##### POST /users/{user_id}/pairings

Example: POST /users/fd031a39-a712-4081-a8b6-b72df64a170e/pairings

The call to Finalize Registration completes the registration and pairing process initiated by calling Initiate Registration. During the manual flow, the user enters the OTP manually to the service provider’s application, which calls Finalize Registration while providing this OTP value.

##### Request Body Parameters

```json
{
  "client_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "application_id": "4df5f945-5fe8-48fb-8a5a-0a6b71da42de",
  "otp": "1234567",
  "ip_address": "127.0.0.1",
  "typing_pattern": "",
  "user_data": { ... },
  "device_details": {
    "fingerprint": "bWxjelNMUEEyUzZEQ0J6NFBqcmE=",
    "rooted": "true"
  },
   "auth_data": {
     "type": "public-key",
     "alg": "ECDSA",
     "credential_id": "34cc85a7-7e7d-42e1-9d28-d02811ba834f",
     "credential": "UCNS4cGsexyqxzpGt2bkzWjSC8yp8HEn"
   }
}
```

| Parameter      | Type   | Description                                                  |
| -------------- | ------ | ------------------------------------------------------------ |
| client_id      | String | The identifier of the service provider account.              |
| application_id | String | The identifier of the application, which belongs to this service provider. |
| otp            | String | The one-time password entered by the user. Omitted for `automatic` flow. |
| ip_address     | String | The requesting device’s IP address.                          |
| typing_pattern | String | Typing pattern data as returned by TypingDNA SDK             |
| user_data      | Object | Optional. The value is passed to service provider’s application unchanged. |
| device_details | Object | Device data object (see below).                              |
| auth_data      | Object | Auth data object (see below).                                |

##### Device Details Object

| Parameter   | Type   | Description                                                  |
| ----------- | ------ | ------------------------------------------------------------ |
| fingerprint | String | The requesting device’s fingerprint.                         |
| rooted      | String | Root/jailbreak status, possible values - `true`, `false`, `failed` |

##### Auth Data Object

| Parameter     | Type   | Description                                |
| ------------- | ------ | ------------------------------------------ |
| type          | String | Always `public-key`                        |
| alg           | String | Cryptographic algorithm used for the key   |
| credential_id | String | The identifier of the generated public key |
| credential    | String | The value of the generated publc key       |

##### Response Body Parameters

```json
{
  "request_id": "d5eb5c65-e215-4b83-b097-7b5f5d5e033a",
  "device_id": "d90f0ccb-b174-460e-8274-6524ecef9dc0",
  "status": "ACTIVE"
}
```



### 3. Authentication user flows

##### 3.1. Mobile app authentication

<img src="/Users/znatokov/Desktop/hive/UML Sequence_ User Login Overview - Page 3.png" alt="UML Sequence_ User Login Overview - Page 3" style="zoom:33%;" />



##### 3.2. Out of Band (OOB) authentication, e.g. desktop web authentication

<img src="/Users/znatokov/Desktop/hive/UML Sequence_ User Login Overview - Page 4.png" alt="UML Sequence_ User Login Overview - Page 4" style="zoom:33%;" />



##### 3.3. Start Authentication

##### POST /authentications

##### Request Body Parameters

```json
{
  "client_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "application_id": "4df5f945-5fe8-48fb-8a5a-0a6b71da42de",
  "email": "name@example.com",
  "phone": "+3800000000",
  "ip_address": "127.0.0.1",
  "typing_pattern": "",
  "user_data": { ... },
	"device_id": "d90f0ccb-b174-460e-8274-6524ecef9dc0",
  "type": "MOBILE"
}
```

| Parameter      | Type   | Description                                                  |
| -------------- | ------ | ------------------------------------------------------------ |
| client_id      | String | The identifier of the service provider account.              |
| application_id | String | The identifier of the application, which belongs to this service provider. |
| email          | String | User's email address. Required if `phone` is not provided.   |
| phone          | String | User's phone number. Required if `email` is not provided.    |
| ip_address     | String | The requesting device’s IP address.                          |
| typing_pattern | String | Typing pattern data as returned by TypingDNA SDK             |
| user_data      | Object | Optional. The value is passed to service provider’s application unchanged. |
| device_id      | String | Specify the device with which to authenticate. For web flow can be selected from the available devices. For mobile flow always the ID of current device. |
| type           | String | The type of authentication to be used. Valid values:<br />- `MOBILE`: mobile flow (always default for mobile SDK)<br />- `PUSH`: web flow (used only on web and not needed for mobile SDK) |

##### Response Body Parameters

```json
{
  "request_id": "10643c19-2d8c-4adc-9ab8-cf1b76b4619b",
  "authentication_id": "15295eca-beed-4a3c-946b-a6bc2b0779c8",
  "auth_data": "{\"challenge\": \"kYhXBWX0HO5GstIS02yPJVhiZ0jZLH7PpC4tzJI-ZcA=\",\"timeout\":120000,\"allow_credentials\":[{\"id\":\"34cc85a7-7e7d-42e1-9d28-d02811ba834f\",\"type\":\"public-key\"}],\"user_verification\":\"required\"}",
  "status": "IN_PROGRESS"
}
```

| Parameter         | Type   | Description                                                  |
| ----------------- | ------ | ------------------------------------------------------------ |
| request_id        | String | Unique value to help trace the requests, automatically appended to each response. |
| authentication_id | String | An authentication ID value for this session. Must be used in the matching Complete Authentication call. |
| auth_data         | String | JSON structure containing parameters for retrieving the public key credentials and authentication challenge. |
| status            | String | The status returned by the authentication call. Valid values:<br />- `IN_PROGRESS`: The authentication of the user is in progress |

##### Authentication Data (`auth_data` JSON)

`challenge` - a cryptographically random value generated by the server to mitigate "replay attacks".

`timeout` - the time, in milliseconds, that the auth session will be valid

`allow_credentials` - a list of acceptable public key credentials for this user. An entry includes credential `id` and `type`.

`user_verification` - the default value is `required`.

##### 3.4. Complete Authentication

##### PUT /authentications/{authentication_id}

Example: PUT /authentications/15295eca-beed-4a3c-946b-a6bc2b0779c8

##### Request Body Parameters

```json
{
  "client_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "application_id": "4df5f945-5fe8-48fb-8a5a-0a6b71da42de",
  "ip_address": "127.0.0.1",
  "user_data": { ... },
	"device_id": "d90f0ccb-b174-460e-8274-6524ecef9dc0",
  "device_details": {
    "fingerprint": "bWxjelNMUEEyUzZEQ0J6NFBqcmE=",
    "rooted": "true"
  },
  "auth_data": "{\"id\":\"34cc85a7-7e7d-42e1-9d28-d02811ba834f\",\"authenticator_data\": \"\",\"client_data\":\"\",\"signature\":\"MEUCIQDNrG...\"}"
}
```

| Parameter      | Type   | Description                                                  |
| -------------- | ------ | ------------------------------------------------------------ |
| client_id      | String | The identifier of the service provider account.              |
| application_id | String | The identifier of the application, which belongs to this service provider. |
| session_id     | String | The session_id value received in the response to Start Authentication or Push Notification. |
| ip_address     | String | The requesting device’s IP address.                          |
| user_data      | Object | Optional. The value is passed to service provider’s application unchanged. |
| device_id      | String | Enrolled device ID.                                          |
| device_details | Object | Device data object (see below).                              |
| response       | String | JSON structure containing authentication parameters and signature. |

##### Device Details Object

| Parameter   | Type   | Description                                                  |
| ----------- | ------ | ------------------------------------------------------------ |
| fingerprint | String | The requesting device’s fingerprint.                         |
| rooted      | String | Root/jailbreak status, possible values - `true`, `false`, `failed` |

##### Authentication Data (`auth_data` JSON)

`id` - The identifier for the credential that was used to generate the authentication assertion (signature).

`authenticator_data` - empty for now

`client_data` - empty for now

`signature` - The signature generated by the private key associated with this credential. On the server, the public key will be used to verify that this signature is valid.

##### Response Body Parameters

```json
{
  "request_id": "ede807ee-b91f-4119-ba91-b7e94dfec4a6",
  "status": "APPROVED"
}
```

##### 3.5. Cancel Authentication

##### DELETE /authentications/{authentication_id}

Example: DELETE /authentications/15295eca-beed-4a3c-946b-a6bc2b0779c8

##### Response Body

```json
{
  "request_id": "65698805-4b93-4e57-a0a6-2e56bad84ba7"
}
```

### 4. Devices

##### 4.1. Update a device of a user

##### PATCH /devices/{device_id}

Example: PATCH /devices/d90f0ccb-b174-460e-8274-6524ecef9dc0

##### Request Body Parameters

```json
{
  "client_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "application_id": "4df5f945-5fe8-48fb-8a5a-0a6b71da42de",
  "user_id": "fd031a39-a712-4081-a8b6-b72df64a170e",
  "ip_address": "127.0.0.1",
  "device_details": {
    "fingerprint": "bWxjelNMUEEyUzZEQ0J6NFBqcmE=",
    "rooted": "true"
  },
  "push_token": "ekO1XdQYgk:APA91bHmgm_K500RVhexcxFVoczhp5RuMSKC07kOJB7-T31xq2_a9tkUAFVGQNwtZ2JORj79lDRI0ow-nP17y82GD1zTWJTEnyjNMas_qNUKxBot1P-vM6v-BW7sqcISak8sXMK91WfmH",
  "role": "PRIMARY"
}
```

| Parameter      | Type   | Description                                                  |
| -------------- | ------ | ------------------------------------------------------------ |
| client_id      | String | The identifier of the service provider account.              |
| application_id | String | The identifier of the application, which belongs to this service provider. |
| user_id        | String | The session_id value received in the response to Start Authentication or Push Notification. |
| ip_address     | String | The requesting device’s IP address.                          |
| device_details | Object | Device data object.                                          |
| push_token     | String | Device token or registration ID for APNS/FCM/GCM.            |
| role           | String | Role of the device if user has more than one device. Valid values:<br />- `PRIMARY`<br />- `TRUSTED` |

##### Response Body

```json
{
  "request_id": "aa445e7b-30f8-4b1f-bc87-09a8f45f8d7f",
  "device_id": "d90f0ccb-b174-460e-8274-6524ecef9dc0",
  "push_token": "ekO1XdQYgk:APA91bHmgm_K500RVhexcxFVoczhp5RuMSKC07kOJB7-T31xq2_a9tkUAFVGQNwtZ2JORj79lDRI0ow-nP17y82GD1zTWJTEnyjNMas_qNUKxBot1P-vM6v-BW7sqcISak8sXMK91WfmH",
  "role": "PRIMARY"
}
```

##### 4.2. Push Notifications

In order to use push notifications, customer will have to provide the following using the admin console:

- Apple iOS Push Services Certificate for iOS
- FCM/GCM Sender ID and server key for Android

Payload in the push notification is similar to that returned by 3.3. Start authentication. Example for APNS:

```json
{
	"aps" : 
	{
		"alert" : "You have a notification",
		"badge" : 1,
		"sound" : "default"
	},
  "hivekey": true,
  "authentication_id": "15295eca-beed-4a3c-946b-a6bc2b0779c8",
  "auth_data": "{\"challenge\": \"kYhXBWX0HO5GstIS02yPJVhiZ0jZLH7PpC4tzJI-		ZcA=\",\"timeout\":120000,\"allow_credentials\":[{\"id\":\"34cc85a7-7e7d-42e1-9d28-d02811ba834f\",\"type\":\"public-key\"}],\"user_verification\":\"required\"}"
}
```

`hivekey` is used to help the Service provider (Mobile application) to determine that the notification must be routed to the SDK, in case the app is configured to receive different user notifications. 

### 5. Errors

Error representation:

```json
{
  "request_id": "d7b4b43d-858b-4257-9c7b-9d6e434011af",
  "error_code": "FORBIDDEN",
  "error_details": "APPLICATION_DISABLED" 
}
```

Possible variations of `error_code` and `error_details` are described below.

Error codes:

| Error Code       | HTTP Status Code | Description                                                  |
| ---------------- | :--------------: | ------------------------------------------------------------ |
| FORBIDDEN        |       403        | This status can be returned when service has been disabled.  |
| INVALID_DATA     |       400        | A valid request was received, however the request could not be completed as there was one or more validation errors. |
| INVALID_REQUEST  |       400        | The request was invalid and could not be executed. For example a POST request was made without a body or missing parameter. |
| NOT_FOUND        |       404        | The requested resource does not exist.                       |
| REQUEST_FAILED   |       400        | A valid request was received, however the operation could not be completed due to an issue during the request. |
| UNAUTHORIZED     |       401        | The SDK does not have valid authentication credentials to complete the request. |
| UNEXPECTED_ERROR |       500        | An unexpected server error occurred.                         |

 Detailed error codes:

| Error details        |                                                              |
| -------------------- | ------------------------------------------------------------ |
| APPLICATION_DISABLED | This application is marked as disabled. Refers to service provider `application_id`. |
| CANCELED             | Returned when an action on a resource failed, because the resource is canceled. For example, attempting to authenticate out of band through Push notification but the user already canceled the operation on the web. |
| CLAIMED              | Returned when an action on a resource failed, because the resource is claimed. For example, duplicate use of the pairing code. |
| DEVICE_BLOCKED       | When a user tries to authenticate from an accessing device, but this device is marked as blocked. |
| DEVICE_ROOTED        | The user’s device is detected as having been rooted or jailbroken AND the policy doesn't allow it. |
| INVALID_CLIENT       | Request is made on / by a client that is not verified or has been suspended. Refers to service provider `client_id`. |
| INVALID_VALUE        | Value is invalid due to validation rules.                    |
| REQUIRED_VALUE       | Required attribute was omitted in request.                   |
| SERVICE_ERROR        | An error with the service occurred.                          |
| USER_DISABLED        | User marked as suspended.                                    |
| USER_NOT_ACTIVE      | Returned when an action failed, because the user is not active or hasn't been enrolled (has no devices). |
| EMAIL_NOT_ENABLED    | A user tried to enroll using an email address when it is disabled for the application. |
| PHONE_NOT_ENABLED    | A user tried to enroll using a phone number when it is disabled for the application. |

### 6. Segment

##### 6.1. User triggered events to track with Segment:

manualFlowStart

manualSignupInitiate

manualSignupFinish

manualSignupComplete

manualSignupError

autoFlowStart

autoSignupInitiate

autoSignupFinalize

autoSignupComplete

autoSignupError

authFlowStart

authInitiate

authFinish

authError

biometricAuthStart

biometricAuthError

pushReceived

qrCodeScanned

 

##### 6.2. Properties to append to tracking:

appName (string)

appPackageName (string)

currentLocale (string)

deviceInfo (object)

fontSizeScale (string)

googlePlayServicesVersion (string)

sdkLocaleCode (string)

sdkVersion (string)